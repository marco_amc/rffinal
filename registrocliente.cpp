#include "registrocliente.h"
#include "ui_registrocliente.h"
#include <QDebug>
#include <cliente.h>
registroCliente::registroCliente(QSqlDatabase*db,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::registroCliente)
{
    this->db=*db;
    ui->setupUi(this);
    qDebug()<<"Creando registro";
}

registroCliente::~registroCliente()
{
    delete ui;
}

void registroCliente::on_btnGuardarCliente_clicked()
{
    //coneccion con la base
    db=QSqlDatabase::addDatabase("QODBC");
    db.setDatabaseName("DRIVER={MYSQL ODBC 8.0 UNICODE DRIVER}; SERVER=127.0.0.1; uid=root; pwd="";  Database=mydb;");
    QSqlQuery query;
    //condicional que evita campos vacios
    if(!vacio(ui->txtnombre->text()) && !vacio(ui->txtpaterno->text())  && !vacio(ui->txtmaterno->text())
    &&  !vacio(ui->txtCalle->text())  &&  !vacio(ui->txtColonia->text()) &&  !vacio(ui->txtCp->text())
    && !vacio(ui->txtTelefono->text()) && !vacio(ui->txtCuentaB->text()) && !vacio(ui->nombreUsuario->text())
    && !vacio(ui->passUsu->text())){
        cliente *c=new cliente();
        c->setNombre(ui->txtnombre->text());
        c->setPaterno(ui->txtpaterno->text());
        c->setMaterno(ui->txtmaterno->text());
        c->setCalle(ui->txtCalle->text());
        c->setColonia(ui->txtColonia->text());
        c->setCp(ui->txtCp->text().toInt());
        c->setTelefono(ui->txtTelefono->text().toInt());
        c->setNumCuenta(ui->txtCuentaB->text().toInt());
        c->setNombreUsu(ui->nombreUsuario->text());
        c->setPass(ui->passUsu->text());
        if(db.open()){      //prpare es muy util para incerciones
            query.prepare("insert into cliente(nombre,apaterno,amaterno,calle,colonia,cp,telefono,nuCuenta,"
            "nombreusu,password)values(?,?,?,?,?,?,?,?,?,?)");
            query.addBindValue(c->getNombre());
            query.addBindValue(c->getPaterno());
            query.addBindValue(c->getMAterno());
            query.addBindValue(c->getCalle());
            query.addBindValue(c->getColonia());
            query.addBindValue(c->getCp());
            query.addBindValue(c->getTelefono());
            query.addBindValue(c->getNumCuenta());
            query.addBindValue(c->getNombreUsu());
            query.addBindValue(c->getPass());
            if(query.exec()){
                qDebug()<<"funciono";
                QMessageBox::information(this,"Exito","Registro exitoso!");//notificacion exito
                this->setVisible(false);                                    //desaparece el formulario
            }else{
                qDebug()<<"nel query";
            }
        }else{
            qDebug()<<"nel base";
        }

    }else {     //Algun campo esta vacio
        QMessageBox msgBox;
        msgBox.setWindowTitle("Aviso");
        msgBox.setText("Algun campo esta incompleto");
        QPushButton *connectButton = msgBox.addButton(tr("Aceptar"), QMessageBox::ActionRole);
    }
}
bool registroCliente::vacio(QString str){
    if (str=="") {
        return true;
    }else{
        return false;
    }
}

void registroCliente::on_cancelarR_clicked()
{
    this->close();
}
