#ifndef INTERFAZ_H
#define INTERFAZ_H
//GuardadoMarco
#include <QMainWindow>
#include<QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QLineEdit>
#include <propietario.h>
#include <dialog.h>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
//#include <QSqlError>
#include <QSql>
#include <QtSql/QSqlQuery>
#include <QMessageBox>
#include <QDebug>
#include <QPixmap>
#include <QBuffer>
#include <QFileDialog>
#include <QVideoWidget>
#include <casa.h>
namespace Ui {
class interfaz;
}
class QVideoWidget;

class interfaz : public QMainWindow
{
    Q_OBJECT

public:
    explicit interfaz(QWidget *parent = nullptr);
    ~interfaz();
    bool labelVacio(QString);
    QString getBuscar();
    void vaciarPropiedades();
    void agregarPropiedad1(Casa*,bool);
    void agregarPropiedad2(Casa*,bool);
    void agregarPropiedad3(Casa*,bool);
    void agregarPropiedad4(Casa*,bool);
    void agregarPropiedad5(Casa*,bool);
    void agregarPropiedad6(Casa*,bool);
    void agregarPropiedad7(Casa*,bool);
    void agregarPropiedad8(Casa*,bool);
    void agregarImagenBotonesCasa(int,int);
    void primeraPagina();
private slots:
    void on_confirmarR_4_clicked();
    void on_registrate_clicked();

    void on_iniciaSesion_clicked();

    void on_actionSalir_triggered();

    void on_generarCont_4_clicked();
    QString generadorContrasena(int);

    void VerificarCampos();



    void MensajeErrorCampos();

    void on_botonprincipal_accepted();

    void on_botonprincipal_rejected();

    void on__subirfoto_clicked();

    void Verestado();

    void on_verCatalogo_clicked();

    void on_siguiente_clicked();

    void on_anterior_clicked();

    void on_buscar_clicked();

private:
    Ui::interfaz *ui;
    QSqlDatabase db;
    int IdEnuso; //
    bool RegTerminado;
    bool fot;
    QString datos[12];
    QVideoWidget *mVideo;
    Casa*casa[100];
    Casa*busqueda[100];
    int contador,pagina,paginaActual;
};
/*
 1-> calle
 2-> colonia
 3->cp
 4 estado
 5 actividades
 6 descripcion
 7 estrellas
 8 precio
 9 precio vacaciones
 10 presonas
 11 noches
 12 bool mascota
 13 url imagen falla en el metodo
*/
#endif // INTERFAZ_H
