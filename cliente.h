#ifndef CLIENTE_H
#define CLIENTE_H

#include<QString>
class cliente
{
private:
    QString nombreUsu;
    QString pass;
    QString nombre;
    QString paterno;
    QString materno;
    QString calle;
    QString colonia;
    int cp;
    int telefono;
    int numCuenta;
public:
    cliente();
    //Cliente(QString nombreUsu,QString pass,QString nombre,QString paterno,QString materno,QString calle,QString colonia,int cp,int telefono,int numCuenta);
    void setNombreUsu(QString aux){nombreUsu=aux;}
    void setPass(QString aux){pass=aux;}
    void setNombre(QString aux){nombre=aux;}
    void setPaterno(QString aux){paterno=aux;}
    void setMaterno(QString aux){materno=aux;}
    void setCalle(QString aux){calle=aux;}
    void setColonia(QString aux){colonia=aux;}
    void setCp(int aux){cp=aux;}
    void setTelefono(int aux){telefono=aux;}
    void setNumCuenta(int aux){numCuenta=aux;}
    QString getNombreUsu(){return nombreUsu;}
    QString getPass(){return pass;}
    QString getNombre(){return nombre;}
    QString getPaterno(){return paterno;}
    QString getMAterno(){return materno;}
    QString getCalle(){return calle;}
    QString getColonia(){return colonia;}
    int getCp(){return cp;}
    int getTelefono(){return telefono;}
    int getNumCuenta(){return numCuenta;}
};

#endif // CLIENTE_H
