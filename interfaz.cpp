#include "interfaz.h"
#include "ui_interfaz.h"
#include <QDebug>
#include <QMessageBox>
#include <registrocliente.h>
#include <QVideoWidget>
interfaz::interfaz(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::interfaz)
{
    ui->setupUi(this);
                                                                    //se conecta con la base
    db=QSqlDatabase::addDatabase("QODBC");
    db.setDatabaseName("DRIVER={MYSQL ODBC 8.0 UNICODE DRIVER}; SERVER=127.0.0.1; uid=root; pwd="";  Database=mydb;");
    ui->mainToolBar->setVisible(0);
    ui->toolBar->setVisible(0);
    if(db.open()){
        qDebug()<<"Conexion exitosa con la base";
    }

    else {
        QMessageBox::critical(this,"Oh no Error",db.lastError().text());

    }
 RegTerminado = false;
 fot = false;
 IdEnuso = 0; // Permite guardar el identificador de el actor

}

interfaz::~interfaz()
{
    delete ui;
}

void interfaz::on_confirmarR_4_clicked()
{
    if(!labelVacio(ui->nombre->text()) && !labelVacio(ui->apaterno->text()) && !labelVacio(ui->amaterno->text()) &&
    !labelVacio(ui->calle->text()) && !labelVacio(ui->colonia->text()) && !labelVacio(ui->cp->text()) && !labelVacio(ui->telefono->text())
     && !labelVacio(ui->usuario->text()) && !labelVacio(ui->tarjeta->text()) && !labelVacio(ui->pass->text())){

       // Dialog *d=new Dialog();               //confirmacion de registro
       // d->exec();


        propietario *p=new propietario();                  //Se crea un objeto propietario para almacenar la info del formulario
        p->setNombre(ui->nombre->text());
        p->setApellidoP(ui->apaterno->text());
        p->setApellidoM(ui->amaterno->text());
        p->setCalle(ui->calle->text());
        p->setColonia(ui->colonia->text());
        p->setCp(ui->cp->text());
        p->setTelefono(ui->telefono->text().toInt());
        p->setUsuario(ui->usuario->text());
        p->setNoCuenta(ui->tarjeta->text().toInt());
        p->setPass(ui->pass->text());
        if(db.open()){                                     //se prueba que se conecte con la base de datos
            qDebug()<<"La conexion ha sido exitosa";
            QSqlQuery query;
            query.prepare("insert into propietario(nombre,apaterno,amaterno,calle,colonia,cp,telefono,noCuenta,idAdministrador,nombreusu,password)"
                          "values(?,?,?,?,?,?,?,?,?,?,?) ");                //se prepara el query ,aun no se ejecuta
            query.addBindValue(p->getNombre());                           //se agregan los campos en el orden en el que se ejecuta addBindValue
            query.addBindValue(p->getApellidoP());
            query.addBindValue(p->getApellidoM());
            query.addBindValue(p->getCalle());
            query.addBindValue(p->getColonia());
            query.addBindValue(p->getCp());
            query.addBindValue(p->getTelefono());
            query.addBindValue(p->getNoCuenta());
            query.addBindValue(1);                  //esto esta trunco,debe jalar el id del administrador actual
            query.addBindValue(p->getUsuario());
            query.addBindValue(p->getPass());

            if(query.exec()){
                QMessageBox::information(this,"Exito","El propietario se registro con exito");
                qDebug()<<"Query funciono";

            }else {
                qDebug()<<"Query fallo";
                qDebug()<<query.lastError().text();
            }
       }
    }else {//algun campo esta vacio
        QMessageBox msgBox;

        msgBox.setWindowTitle("Aviso");
        msgBox.setText("Algun campo esta vacio");
        QPushButton *connectButton = msgBox.addButton(tr("Aceptar"), QMessageBox::ActionRole);
        //QPushButton *abortButton = msgBox.addButton("No",QMessageBox::ActionRole);

        msgBox.exec();
    }

}

bool interfaz::labelVacio(QString l){
    if(l==""){
        return true;
    }else {
        return false;
    }
}
void interfaz::on_registrate_clicked()
{
    registroCliente r(&db);
    r.exec();
}

void interfaz::on_iniciaSesion_clicked()
{
    bool usuarioEncontrado=false;
    bool propietarioEncontrado = false;
        QSqlQuery q;
        if(q.exec("select*from administrador;"))
        {

            while(q.next()){

                if(q.value(1).toString()== ui->nombreUser->text() && q.value(2).toString() == ui->passW->text()){
                    IdEnuso = q.value(0).toInt();
                    usuarioEncontrado=true;
                }
                }
                //qDebug() << "El bool del administrador es" << usuarioEncontrado;

            }
        else
        {
            QMessageBox::information(this,"ErrorBaseQuery","Revisa tu query");
        }

      QSqlQuery p;
      if(p.exec("select * from propietario;")){
          while(p.next()){
             // qDebug() << p.value(10);
              if(p.value(10).toString() == ui->nombreUser->text() && p.value(11).toString() == ui->passW->text()){
                  IdEnuso = p.value(0).toInt();
              propietarioEncontrado = true;
              }
          }
      }
      else
      {
          QMessageBox::information(this,"ErrorBaseQuery","Revisa tu query");
      }

      if(!usuarioEncontrado && !propietarioEncontrado){
        QMessageBox::information(this,"El usuario no existe","Verifica el usuario y contraseña");
        ui->nombreUser->clear();
        ui->passW->clear();
        }
    else{
    if(usuarioEncontrado){
    qDebug() << "Estoy en admi";
    ui->carril->setCurrentIndex(0);
    ui->mainToolBar->setVisible(1);
    ui->toolBar->setVisible(0);

}
if(propietarioEncontrado){
    qDebug() << "Estoy en propietario alv";
    ui->carril->setCurrentIndex(1);
    ui->toolBar->setVisible(1);
    ui->mainToolBar->setVisible(0);
    QSqlQuery *NombrePro = new QSqlQuery();
    QString id = QString::number(IdEnuso);
 qDebug()<< "select nombre,apaterno,amaterno From propietario where idPropietario="+id+";"; // Se pone el id 2 para probar
 NombrePro->exec("select nombre,apaterno,amaterno From propietario where idPropietario="+id +";");
 NombrePro->next();
 qDebug()<<NombrePro->value(0)<<NombrePro->value(1)<<NombrePro->value(2);
 QString name = NombrePro->value(0).toString();
 QString apelli = NombrePro->value(1).toString();
 QString apelli2 = NombrePro->value(2).toString(); // Se dividio en 3 para hacer pruebas
 QString NombreCompleto = name+apelli+apelli2;

 delete NombrePro;
    }
ui->nombreUser->clear();
ui->passW->clear();
 }
}


void interfaz::on_actionSalir_triggered()
{
    ui->carril->setCurrentIndex(2);
    ui->mainToolBar->setVisible(0);
    ui->toolBar->setVisible(0);
}

void interfaz::on_generarCont_4_clicked()
{
    ui->pass->setText(generadorContrasena(6));
}

QString interfaz::generadorContrasena(int len){
    QString str;
    str.resize(len);
    for (int s = 0; s < len ; ++s)
        str[s] = QChar('A' + char(qrand() % ('Z' - 'A')));

    return str;
}
/*_------------------------------------------------------------------------------------*/
void interfaz::MensajeErrorCampos()
{
    QMessageBox::information(nullptr,"Ojo","Todos los campos son obligatorios verifica");
}




void interfaz::VerificarCampos(){
      datos[0] = ui->_calle->text();
    datos[1] = ui->_col->text();
     datos[2] = ui->_cp->text();
     datos[3] = QString::number(ui->estado->currentIndex()); // Este devuel
     datos[4] = ui->_act->toPlainText();
        datos[5] = ui->_des->toPlainText();
        datos[6] = ui->_estrella->text();
     datos[7] = ui->_precionoche->text();
     datos[8] = ui->_tarifavacaciones->text();
     datos[9] = ui->_personas->text();
     datos[10] = ui->_nochesmin->text();
     if(ui->_mascotassi->isChecked())
         datos[11] = "1";
     else
         datos[11] = "0";
             if(datos[0] != ""){
                 qDebug ()<< "0 Si";
                if(datos[1] != ""){
                    qDebug ()<< "1 Si";
                     if(datos[2] != ""){
                         qDebug ()<< "2 Si";
                             if(datos[3] != ""){
                                 qDebug ()<< "3 Si";
                                 if(datos[4] != ""){
                                     qDebug ()<< "4 Si";
                                     if(datos[5] != ""){
                                         qDebug ()<< "5 Si";
                                         if(datos[7] != ""){
                                             qDebug ()<< "7 Si";
                                             if(datos[8] != ""){
                                                 qDebug ()<< "8 Si";
                                                 if(fot)
                                                 RegTerminado = true;
                                                 else
                                                     QMessageBox::information(this,"Informacion","Tienes que subir almenos una fotografia");
                                             }
                                             else
                                                 MensajeErrorCampos();
                                         }
                                         else
                                             MensajeErrorCampos();
                                     }
                                     else
                                         MensajeErrorCampos();
                                 }
                                 else
                                     MensajeErrorCampos();
                }
                else
                    MensajeErrorCampos();//Mensaje4
            }else
                MensajeErrorCampos();// Mensaje 3

        }else
            MensajeErrorCampos();//Mensaje 2

        }
        else
            MensajeErrorCampos(); // Mensaje 1
}


void interfaz::on_botonprincipal_accepted()
{
    // Si ya estan terminadas todas las ventanas se guarda la informacion.
    VerificarCampos();
    if(RegTerminado){
        Verestado();
        for(int a = 0; a <12; a++ )
            qDebug() << datos[a] << "\n";
        // Subir a base de datos
         QSqlQuery *insercion1 = new QSqlQuery();
         insercion1->prepare("INSERT INTO casa(calle,colonia,cp,descripcion,petfriendly,nestrellas,idPropietario,idEstado,nopersonas,precio,tarifavacacional,actividades,minNoches,img)"
                            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
         insercion1->addBindValue(datos[0]);
         insercion1->addBindValue(datos[1]);
         insercion1->addBindValue(datos[2]);
         insercion1->addBindValue(datos[5]);
         insercion1->addBindValue(datos[11]);
         insercion1->addBindValue(datos[6]);
         insercion1->addBindValue(IdEnuso);
         insercion1->addBindValue(datos[3]); // Falta agregar lo del estado ; Solo es un query pero ya hay que tener registrados los estados
         insercion1->addBindValue(datos[10]);
         insercion1->addBindValue(datos[7]);
         insercion1->addBindValue(datos[8]);
          insercion1->addBindValue(datos[4]);
           insercion1->addBindValue(datos[9]);
           insercion1->addBindValue(datos[12]);
           if(insercion1->exec()){
               QMessageBox::information(this,"Exito","Se agrego la propiedad");
               delete insercion1;
               ui->carril->setCurrentIndex(2);
               ui->mainToolBar->setVisible(0);
               ui->toolBar->setVisible(0);
               // Puede que haya error
           }
           else
                  QMessageBox::information(this,"Rayos",insercion1->lastError().text());
    }
    else{
        QMessageBox::information(this,"Informacion","No has completado el registro");
    }
}

void interfaz::on_botonprincipal_rejected()
{
    QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Cuidado", "Seguro que quieres cancelar el registro",
                                    QMessageBox::Yes|QMessageBox::No);
      if (reply == QMessageBox::Yes) {
          qDebug() << "Salida";
          ui->carril->setCurrentIndex(2);
          ui->mainToolBar->setVisible(0);
          ui->toolBar->setVisible(0);
        } else {
          qDebug() << "Salida Cancelada";
        }
}
/*-----------------------------------------------------------------------------------*/

void interfaz::on__subirfoto_clicked()
{
    datos[12] = QFileDialog::getOpenFileName(this, tr("Seleccione una Imagen"),"/home/",
                                             tr("Images (*.png *.xpm *.jpg)"));
   // datos[12] = u;
    QPixmap pixmap;
    pixmap.load(datos[12]);
    //linea->setText(u);
    QByteArray bytes;
    QBuffer buffer(&bytes);
    buffer.open(QIODevice::WriteOnly);
    pixmap.save(&buffer, "PNG");
    if (bytes.isNull() )
    {
    QMessageBox::warning(this, "Error","No se Puede ver la Imagen \n"
    "Ha Elegido Cancelar");
    }else{
    ui->imagen1_3->setPixmap(pixmap);
    fot= true;
    qDebug () << "Pasa la fotograafica sin problema " << fot;
    }
}

void interfaz::Verestado()
{
    for(int i = 0; i < 32; i++){
        qDebug() << ui->estado->currentIndex();
        qDebug() << ui->estado->currentText();
    }
}
QString interfaz::getBuscar(){
    return ui->lineEdit->text();
}

void interfaz::vaciarPropiedades()
{
    Casa *casa=new Casa();
    agregarPropiedad1(casa,1);
    agregarPropiedad2(casa,1);
    agregarPropiedad3(casa,1);
    agregarPropiedad4(casa,1);
    agregarPropiedad5(casa,1);
    agregarPropiedad6(casa,1);
    agregarPropiedad7(casa,1);
    agregarPropiedad8(casa,1);

}
//funciones para agregar datos a los label y botones en el catalogo o limpiar
void interfaz::agregarPropiedad1(Casa *casa,bool vaciar)
{
    ui->titulo1->setText(casa->getTitulo());
    ui->descripcion1->setText(casa->getDescripcion());
    ui->precio1->setText(casa->getPrecioNormal());
    ui->calificacion1->setText(casa->getNEstrellas());
    ui->pro1->setVisible(1);
        ui->estrella1->setText("");
    if(vaciar){
        ui->pro1->setVisible(0);
        ui->titulo1->setText("");
        ui->descripcion1->setText("");
        ui->precio1->setText("");
        ui->calificacion1->setText("");
        ui->estrella1->setText("");
    }
  //  this->setStyleSheet("#pro1{border-image:url('"+casa->getImg()+"'); border-radius:4px}");

}
void interfaz::agregarPropiedad2(Casa *casa,bool vaciar)
{
    ui->titulo2->setText(casa->getTitulo());
    ui->descripcion2->setText(casa->getDescripcion());
    ui->precio2->setText(casa->getPrecioNormal());
    ui->calificacion2->setText(casa->getNEstrellas());
    ui->pro2->setVisible(1);
    ui->estrella2->setText("");
    if(vaciar){
        ui->pro2->setVisible(0);
        ui->titulo2->setText("");
        ui->descripcion2->setText("");
        ui->precio2->setText("");
        ui->calificacion2->setText("");
        ui->estrella2->setText("");
    }
}
void interfaz::agregarPropiedad3(Casa *casa,bool vaciar)
{
    ui->titulo3->setText(casa->getTitulo());
    ui->descripcion3->setText(casa->getDescripcion());
    ui->precio3->setText(casa->getPrecioNormal());
    ui->calificacion3->setText(casa->getNEstrellas());
    ui->pro3->setVisible(1);
    ui->estrella3->setText("");
    if(vaciar){
        ui->pro3->setVisible(0);
        ui->titulo3->setText("");
        ui->descripcion3->setText("");
        ui->precio3->setText("");
        ui->calificacion3->setText("");
        ui->estrella3->setText("");
    }
}
void interfaz::agregarPropiedad4(Casa *casa,bool vaciar)
{
    ui->titulo4->setText(casa->getTitulo());
    ui->descripcion4->setText(casa->getDescripcion());
    ui->precio4->setText(casa->getPrecioNormal());
    ui->calificacion4->setText(casa->getNEstrellas());
    ui->pro4->setVisible(1);
    ui->estrella4->setText("");
    if(vaciar){
        ui->titulo4->setText("");
        ui->descripcion4->setText("");
        ui->precio4->setText("");
        ui->calificacion4->setText("");
        ui->pro4->setVisible(0);
        ui->estrella4->setText("");
    }
}
void interfaz::agregarPropiedad5(Casa *casa,bool vaciar)
{
    ui->titulo5->setText(casa->getTitulo());
    ui->descripcion5->setText(casa->getDescripcion());
    ui->precio5->setText(casa->getPrecioNormal());
    ui->calificacion5->setText(casa->getNEstrellas());
    ui->pro5->setVisible(1);
    ui->estrella5->setText("");

    if(vaciar){
        ui->titulo5->setText("");
        ui->descripcion5->setText("");
        ui->precio5->setText("");
        ui->calificacion5->setText("");
        ui->pro5->setVisible(0);
            ui->estrella5->setText("");
    }
}
void interfaz::agregarPropiedad6(Casa *casa,bool vaciar)
{
    ui->titulo6->setText(casa->getTitulo());
    ui->descripcion6->setText(casa->getDescripcion());
    ui->precio6->setText(casa->getPrecioNormal());
    ui->calificacion6->setText(casa->getNEstrellas());
    ui->pro6->setVisible(1);
        ui->estrella6->setText("");
    if(vaciar){
        ui->titulo6->setText("");
        ui->descripcion6->setText("");
        ui->precio6->setText("");
        ui->calificacion6->setText("");
        ui->pro6->setVisible(0);
            ui->estrella6->setText("");
    }
}
void interfaz::agregarPropiedad7(Casa *casa,bool vaciar)
{
    ui->titulo7->setText(casa->getTitulo());
    ui->descripcion7->setText(casa->getDescripcion());
    ui->precio7->setText(casa->getPrecioNormal());
    ui->calificacion7->setText(casa->getNEstrellas());
    ui->pro7->setVisible(1);
        ui->estrella7->setText("");
    if(vaciar){
        ui->pro7->setVisible(0);
        ui->titulo7->setText("");
        ui->descripcion7->setText("");
        ui->precio7->setText("");
        ui->calificacion7->setText("");
            ui->estrella7->setText("");
    }
}
void interfaz::agregarPropiedad8(Casa *casa,bool vaciar)
{
    ui->titulo8->setText(casa->getTitulo());
    ui->descripcion8->setText(casa->getDescripcion());
    ui->precio8->setText(casa->getPrecioNormal());
    ui->calificacion8->setText(casa->getNEstrellas());
    ui->pro8->setVisible(1);
        ui->estrella8->setText("");
    if(vaciar){
        ui->pro8->setVisible(0);
        ui->titulo8->setText("");
        ui->descripcion8->setText("");
        ui->precio8->setText("");
        ui->calificacion8->setText("");
            ui->estrella8->setText("");
    }
}



void interfaz::agregarImagenBotonesCasa(int contador,int indice)
{
    if(contador==1)
    this->setStyleSheet("#pro1{border-image:url('"+casa[indice-1]->getImg()+"'); border-radius:4px}");
        if(indice==2)
        this->setStyleSheet("#pro1{border-image:url('"+casa[contador-2]->getImg()+"'); border-radius:4px}"
                            "#pro2{border-image:url('"+casa[contador-1]->getImg()+"'); border-radius:4px}");
        if(indice==3)
            this->setStyleSheet("#pro1{border-image:url('"+casa[indice-3]->getImg()+"'); border-radius:4px}"
                                "#pro2{border-image:url('"+casa[indice-2]->getImg()+"'); border-radius:4px}"
                                "#pro3{border-image:url('"+casa[indice-1]->getImg()+"'); border-radius:4px}");
        if(indice==4)
            this->setStyleSheet("#pro1{border-image:url('"+casa[indice-4]->getImg()+"'); border-radius:4px}"
                                "#pro2{border-image:url('"+casa[indice-3]->getImg()+"'); border-radius:4px}"
                                "#pro3{border-image:url('"+casa[indice-2]->getImg()+"'); border-radius:4px}"
                                "#pro4{border-image:url('"+casa[indice-1]->getImg()+"'); border-radius:4px}");
        if(indice==5)
            this->setStyleSheet("#pro1{border-image:url('"+casa[0]->getImg()+"'); border-radius:4px}"
                                "#pro2{border-image:url('"+casa[1]->getImg()+"'); border-radius:4px}"
                                "#pro3{border-image:url('"+casa[2]->getImg()+"'); border-radius:4px}"
                                "#pro4{border-image:url('"+casa[3]->getImg()+"'); border-radius:4px}"
                                "#pro5{border-image:url('"+casa[4]->getImg()+"'); border-radius:4px}");
        if(indice==6)
            this->setStyleSheet("#pro1{border-image:url('"+casa[0]->getImg()+"'); border-radius:4px}"
                                "#pro2{border-image:url('"+casa[1]->getImg()+"'); border-radius:4px}"
                                "#pro3{border-image:url('"+casa[2]->getImg()+"'); border-radius:4px}"
                                "#pro4{border-image:url('"+casa[3]->getImg()+"'); border-radius:4px}"
                                "#pro5{border-image:url('"+casa[4]->getImg()+"'); border-radius:4px}"
                                "#pro6{border-image:url('"+casa[5]->getImg()+"'); border-radius:4px}");
        if(indice==7)
            this->setStyleSheet("#pro1{border-image:url('"+casa[0]->getImg()+"'); border-radius:4px}"
                                "#pro2{border-image:url('"+casa[1]->getImg()+"'); border-radius:4px}"
                                "#pro3{border-image:url('"+casa[2]->getImg()+"'); border-radius:4px}"
                                "#pro4{border-image:url('"+casa[3]->getImg()+"'); border-radius:4px}"
                                "#pro5{border-image:url('"+casa[4]->getImg()+"'); border-radius:4px}"
                                "#pro6{border-image:url('"+casa[5]->getImg()+"'); border-radius:4px}"
                                "#pro7{border-image:url('"+casa[6]->getImg()+"'); border-radius:4px}");



        if(indice==8)
            this->setStyleSheet("#pro1{border-image:url('"+casa[0]->getImg()+"'); border-radius:4px}"
                                "#pro2{border-image:url('"+casa[1]->getImg()+"'); border-radius:4px}"
                                "#pro3{border-image:url('"+casa[2]->getImg()+"'); border-radius:4px}"
                                "#pro4{border-image:url('"+casa[3]->getImg()+"'); border-radius:4px}"
                                "#pro5{border-image:url('"+casa[4]->getImg()+"'); border-radius:4px}"
                                "#pro6{border-image:url('"+casa[5]->getImg()+"'); border-radius:4px}"
                                "#pro7{border-image:url('"+casa[6]->getImg()+"'); border-radius:4px}"
                                                                             "#pro8{border-image:url('"+casa[7]->getImg()+"'); border-radius:4px}");
}

void interfaz::primeraPagina()
{
    ui->anterior->setVisible(0);
    contador=0;
    if(db.open()){
        //funcion para limpiar labels y botones
        vaciarPropiedades();
        QSqlQuery query;
        if(query.exec("select idCasa,descripcion,nestrellas,precio,actividades,img from casa")){
            while(query.next()){
                casa[contador]=new Casa(query.value(0).toInt(),query.value(1).toString(),query.value(2).toInt(),
                                        query.value(3).toDouble()
                                        ,query.value(4).toString(),query.value(5).toString());
                //Se llama a las funciones para asignar texto e imagen a los label
                if(contador==0)
                    agregarPropiedad1(casa[contador],0);
                if(contador==1)
                    agregarPropiedad2(casa[contador],0);
                if(contador==2)
                    agregarPropiedad3(casa[contador],0);
                if(contador==3)
                    agregarPropiedad4(casa[contador],0);
                if(contador==4)
                    agregarPropiedad5(casa[contador],0);
                if(contador==5)
                    agregarPropiedad6(casa[contador],0);
                if(contador==6)
                    agregarPropiedad7(casa[contador],0);
                if(contador==7)
                    agregarPropiedad8(casa[contador],0);
                contador++;
            }
            pagina=(contador/8);
            paginaActual=pagina;
            qDebug()<<pagina;

            agregarImagenBotonesCasa(contador-pagina,contador-pagina);


        }
        else{
            qDebug()<<"Ha ocurrido un error con el catalogo";
        }

    }
}

void interfaz::on_verCatalogo_clicked()
{
    ui->carril->setCurrentIndex(2);
  //  Casa *casa[8];
    primeraPagina();

}

void interfaz::on_siguiente_clicked()
{
    vaciarPropiedades();
    ui->anterior->setVisible(1);
    qDebug()<<contador/pagina<<"Algo";
    if((contador/8)-1==0)
        agregarPropiedad1(casa[contador-1],0);
    if(contador==1)
        agregarPropiedad2(casa[contador],0);
    if(contador==2)
        agregarPropiedad3(casa[contador],0);
    if(contador==3)
        agregarPropiedad4(casa[contador],0);
    if(contador==4)
        agregarPropiedad5(casa[contador],0);
    if(contador==5)
        agregarPropiedad6(casa[contador],0);
    if(contador==6)
        agregarPropiedad7(casa[contador],0);
    if(contador==7)
        agregarPropiedad8(casa[contador],0);
    agregarImagenBotonesCasa(pagina,contador);

}

void interfaz::on_anterior_clicked()
{
    primeraPagina();
}

void interfaz::on_buscar_clicked()
{
    ui->anterior->setVisible(0);
    contador=0;
    if(db.open()){
        //funcion para limpiar labels y botones
        vaciarPropiedades();
        QSqlQuery query;

        if(query.exec("select idCasa,descripcion,nestrellas,precio,actividades,img from casa"
                      " inner join estado on casa.idEstado=estado.idEstado where estado.estado='"+getBuscar()+"'")){
           qDebug()<<getBuscar();
            while(query.next()){
                casa[contador]=new Casa(query.value(0).toInt(),query.value(1).toString(),query.value(2).toInt(),
                                        query.value(3).toDouble()
                                        ,query.value(4).toString(),query.value(5).toString());
                //Se llama a las funciones para asignar texto e imagen a los label
                if(contador==0)
                    agregarPropiedad1(casa[contador],0);
                if(contador==1)
                    agregarPropiedad2(casa[contador],0);
                if(contador==2)
                    agregarPropiedad3(casa[contador],0);
                if(contador==3)
                    agregarPropiedad4(casa[contador],0);
                if(contador==4)
                    agregarPropiedad5(casa[contador],0);
                if(contador==5)
                    agregarPropiedad6(casa[contador],0);
                if(contador==6)
                    agregarPropiedad7(casa[contador],0);
                if(contador==7)
                    agregarPropiedad8(casa[contador],0);
                contador++;
            }
            pagina=(contador/8);
            paginaActual=pagina;
            qDebug()<<pagina;

            agregarImagenBotonesCasa(contador-pagina,contador-pagina);


        }
        else{
            qDebug()<<"Ha ocurrido un error con el catalogo";
        }

    }
    db.close();
}
