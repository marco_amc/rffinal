#include "casa.h"

Casa::Casa()
{
    idCasa=0;
    descripcion="";
    nEstrellas=NULL;
    precioNormal=NULL;
    actividades="";
    img="";
}

Casa::Casa(int id, QString desc, int nEst, double precio, QString actividad, QString img)
{
       idCasa=id;
       descripcion=desc;
       nEstrellas=nEst;
       precioNormal=precio;
       actividades=actividad;
       this->img=img;
}

QString Casa::getDescripcion()
{

    return descripcion;
}

QString Casa::getNEstrellas()
{
    return QString::number(nEstrellas);
}

QString Casa::getPrecioNormal()
{
    return QString::number(precioNormal);
}

QString Casa::getTitulo()
{
    return  actividades;
}

QString Casa::getImg()
{
    return img;
}
