#ifndef CASA_H
#define CASA_H
#include<QString>

class Casa
{
private:
    int idCasa;
    QString calle;
    QString colonia;
    QString cp;
    QString descripcion;
    bool petfriendly;
    int nEstrellas;
    //int idPropietario;
    int nPersonas;
    double precioNormal;
    double precioVacacional;
    QString actividades;
    QString minNoches;
    QString img;

public:
    Casa();
    Casa(int,QString,int,double,QString,QString);
    QString getDescripcion();
    QString getNEstrellas();
    QString getPrecioNormal();
    QString getTitulo();
    QString getImg();

};

#endif // CASA_H
