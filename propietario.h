#ifndef PROPIETARIO_H
#define PROPIETARIO_H
#include <QString>

class propietario
{
private:
    int idPropietario,telefono,noCuenta;
    QString nombre,apellidoP,apellidoM,calle,colonia,cp,usuario,pass;
public:
    propietario();
    propietario(int,int,QString,QString,QString,QString,QString,QString);//constructor para el formulario
    //get y set :(
    void setIdPropietario(int aux){idPropietario= aux;}
    void setTelefono(int aux){telefono= aux;}
    void setNoCuenta(int aux){noCuenta= aux;}
    void setNombre(QString aux){nombre= aux;}
    void setApellidoP(QString aux){apellidoP=aux;}
    void setApellidoM(QString aux){apellidoM=aux;}
    void setCalle(QString aux){calle=aux;}
    void setColonia(QString aux){colonia=aux;}
    void setCp(QString aux){cp=aux;}
    void setUsuario(QString aux){usuario=aux;}
    void setPass(QString aux){pass=aux;}
    int getIdPropietario(){return idPropietario;}
    int getTelefono(){return telefono;}
    int getNoCuenta(){return noCuenta;}
    QString getNombre(){return  nombre;}
    QString getApellidoP(){return  apellidoP;}
    QString getApellidoM(){return apellidoM;}
    QString getCalle(){return calle;}
    QString getColonia(){return colonia;}
    QString getCp(){return cp;}
    QString getUsuario(){return usuario;}
    QString getPass(){return pass;}
};

#endif // PROPIETARIO_H
