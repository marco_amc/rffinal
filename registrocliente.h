#ifndef REGISTROCLIENTE_H
#define REGISTROCLIENTE_H

#include <QDialog>
#include <QSqlQuery>
#include <QSqlError>
#include <qmessagebox.h>
namespace Ui {
class registroCliente;
}

class registroCliente : public QDialog
{
    Q_OBJECT

public:
    explicit registroCliente(QSqlDatabase*,QWidget *parent = nullptr);
    ~registroCliente();
    bool vacio(QString);

private slots:
    void on_btnGuardarCliente_clicked();

    void on_cancelarR_clicked();

private:
    Ui::registroCliente *ui;
    QSqlDatabase db;
};

#endif // REGISTROCLIENTE_H
